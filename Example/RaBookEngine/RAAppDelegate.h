//
//  RAAppDelegate.h
//  RaBookEngine
//
//  Created by ljfantin on 04/19/2017.
//  Copyright (c) 2017 ljfantin. All rights reserved.
//

@import UIKit;

@interface RAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
