//
//  main.m
//  RaBookEngine
//
//  Created by ljfantin on 04/19/2017.
//  Copyright (c) 2017 ljfantin. All rights reserved.
//

@import UIKit;
#import "RAAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RAAppDelegate class]));
    }
}
