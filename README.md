# RaBookEngine

[![CI Status](http://img.shields.io/travis/ljfantin/RaBookEngine.svg?style=flat)](https://travis-ci.org/ljfantin/RaBookEngine)
[![Version](https://img.shields.io/cocoapods/v/RaBookEngine.svg?style=flat)](http://cocoapods.org/pods/RaBookEngine)
[![License](https://img.shields.io/cocoapods/l/RaBookEngine.svg?style=flat)](http://cocoapods.org/pods/RaBookEngine)
[![Platform](https://img.shields.io/cocoapods/p/RaBookEngine.svg?style=flat)](http://cocoapods.org/pods/RaBookEngine)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

pod --skip-import-validation --allow-warnings lib lint

pod repo push personal_specs RaBookEngine.podspec --allow-warnings --use-libraries --skip-import-validation

RaBookEngine is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "RaBookEngine"
```

## Author

ljfantin, lfantin@gmail.com

## License

RaBookEngine is available under the MIT license. See the LICENSE file for more info.
