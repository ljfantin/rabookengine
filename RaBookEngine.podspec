#
# Be sure to run `pod lib lint RaBookEngine.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'RaBookEngine'
  s.version          = '0.1.4'
  s.summary          = 'A short description of RaBookEngine.'

  s.description      = 'engine for rabook'

  s.homepage         = 'https://github.com/ljfantin'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'ljfantin' => 'lfantin@gmail.com' }
  s.source           = { :git => 'https://ljfantin@bitbucket.org/ljfantin/rabookengine.git', :tag => s.version.to_s }
  #s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'
  s.osx.deployment_target = '10.9'
  s.source_files = 'RaBookEngine/Classes/**/*'
  
  # s.resource_bundles = {
  #   'RaBookEngine' => ['RaBookEngine/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'

  s.dependency               'OpenCV', '3.2.0'
end
