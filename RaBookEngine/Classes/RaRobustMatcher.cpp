//
//  RaRobustMatcher.cpp
//  RaBook
//
//  Created by Leandro Fantin on 28/8/16.
//  Copyright © 2016 com.piantao. All rights reserved.
//

#include "RaRobustMatcher.hpp"
#include "RaMatchResult.hpp"
#include "RaFileStorage.hpp"
#include <opencv2/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <cstring>
#include <thread>
#include <iostream>
#include <future>
#include <chrono>


RaRobustMatcher* RaRobustMatcher::instance = NULL;

RaRobustMatcher::RaRobustMatcher()
{
    this->tracking = false;
}

RaRobustMatcher::~RaRobustMatcher()
{
}

RaRobustMatcher * RaRobustMatcher::getRobustMatcherSharedInstance()
{
    if (!instance) {
        instance = new RaRobustMatcher();
    }
    return instance;
}

void RaRobustMatcher::processImages(std::map<std::string ,cv::Mat> &images)
{
    this->queueMutex.lock();

    for (std::map<std::string ,cv::Mat>::iterator it = images.begin() ; it != images.end(); ++it) {
        this->processImage(it->first, it->second);
    }
    
    this->queueMutex.unlock();
}

void RaRobustMatcher::processImage(std::string identifier, cv::Mat &image)
{
    cv::Mat imageOutput;
    image.copyTo(imageOutput);
    preProcess.applyFilter(imageOutput);
    std::vector<cv::KeyPoint> keypoints;
    cv::Mat descriptors;
    this->descriptorFinder.computeImage(imageOutput, keypoints, descriptors);
    
    RaProcessFrame processFrame;
    processFrame.setObjectImageDescriptors(image.cols, image.rows, keypoints, descriptors);
    this->process.insert(std::pair<std::string ,RaProcessFrame>(identifier, processFrame));
}

void RaRobustMatcher::processImageAndSave(cv::Mat &image, std::string &temp)
{
    cv::Mat imageOutput;
    image.copyTo(imageOutput);
    preProcess.applyFilter(imageOutput);
    std::vector<cv::KeyPoint> keypoints;
    cv::Mat descriptors;
    this->descriptorFinder.computeImage(imageOutput, keypoints, descriptors);
    
    RaFileStorage fs;
    fs.saveToFileSystem(temp, keypoints, descriptors);
}

void RaRobustMatcher::addProcessedImage(const std::string &identifier, int width, int height, std::vector<cv::KeyPoint> &keypoints, cv::Mat &descriptors,  LayerType layerType, const std::string &layerIdentifier)
{
    this->queueMutex.lock();
    
    RaProcessFrame processFrame;
    processFrame.setObjectImageDescriptors(width, height, keypoints, descriptors);
    processFrame.setLayerDescriptors(layerType, layerIdentifier);
    this->process.insert(std::pair<std::string ,RaProcessFrame>(identifier, processFrame));
    
    this->queueMutex.unlock();
}

void RaRobustMatcher::removeProcessedImage(std::string & identifier)
{
    this->queueMutex.lock();
    this->process.erase(identifier);
    this->queueMutex.unlock();
}

void RaRobustMatcher::removeAllProcessedImage()
{
    this->queueMutex.lock();
    this->process.clear();
    this->queueMutex.unlock();
}

RaMatchResult RaRobustMatcher::detectAndTrackObject(const cv::Mat &frame)
{
    this->queueMutex.lock();
    RaMatchResult result;
    std::vector<cv::Point2f> points;
    std::vector<cv::Point2f> pointsFrame;
    if (this->isTracking()) {
        result.existsMatch = this->opticalFlow.calculateOpticalFlow(frame, points);
        result.rect = cv::boundingRect(cv::Mat(points));
        result.corners.clear();
        result.identifier = this->layerIdentifier;
        result.layerType = this->layerType;
        std::copy(points.begin(), points.end(), back_inserter(result.corners));
  
    } else {
        cv::Mat descriptors;
        std::vector<cv::KeyPoint> keypoints;
        this->descriptorFinder.computeImage(frame, keypoints, descriptors);

        for (std::map<std::string , RaProcessFrame>::iterator process = this->process.begin() ; process != this->process.end(); ++process) {
        
            std::string identifier = process->first;

            RaMatchResult resultProcess = this->processFrame(frame, identifier, keypoints, descriptors, pointsFrame);
            
            if (resultProcess.existsMatch && resultProcess.stats.matches > result.stats.matches) {
                result = resultProcess;
                points.clear();
                std::copy(pointsFrame.begin(), pointsFrame.end(), back_inserter(points));
            }
        }
        this->opticalFlow.setPreviousPoints(points);
    }
    this->lastResult = result;
    this->opticalFlow.setPreviousFrame(frame);
    this->queueMutex.unlock();
    
    return result;
}

RaMatchResult RaRobustMatcher::detectAndTrackObject(const cv::Mat &frame, const cv::Mat &mask)
{
    this->queueMutex.lock();
    RaMatchResult result;
    std::vector<cv::Point2f> points;
    std::vector<cv::Point2f> pointsFrame;
    if (this->isTracking()) {
        result.existsMatch = this->opticalFlow.calculateOpticalFlow(frame, points);
        result.rect = cv::boundingRect(cv::Mat(points));
        result.corners.clear();
        result.identifier = this->layerIdentifier;
        result.layerType = this->layerType;
        std::copy(points.begin(), points.end(), back_inserter(result.corners));
        
    } else {
        cv::Mat descriptors;
        std::vector<cv::KeyPoint> keypoints;
        this->descriptorFinder.computeImage(frame, mask, keypoints, descriptors);
        
        for (std::map<std::string , RaProcessFrame>::iterator process = this->process.begin() ; process != this->process.end(); ++process) {
            
            std::string identifier = process->first;
            
            RaMatchResult resultProcess = this->processFrame(frame, identifier, keypoints, descriptors, pointsFrame);
            
            if (resultProcess.existsMatch && resultProcess.stats.matches > result.stats.matches) {
                result = resultProcess;
                points.clear();
                std::copy(pointsFrame.begin(), pointsFrame.end(), back_inserter(points));
            }
        }
        this->opticalFlow.setPreviousPoints(points);
    }
    this->lastResult = result;
    this->opticalFlow.setPreviousFrame(frame);
    this->queueMutex.unlock();
    
    return result;
}

RaMatchResult RaRobustMatcher::detectObject(const cv::Mat &frame, const cv::Mat &mask)
{
    this->queueMutex.lock();
    RaMatchResult result;
    std::vector<cv::KeyPoint> keypoints;
    cv::Mat descriptors;

    this->descriptorFinder.computeImage(frame, mask, keypoints, descriptors);
    if (this->isTracking()) {
        result = this->processFrame(frame, this->layerIdentifier, keypoints, descriptors);
    } else {
        for (std::map<std::string , RaProcessFrame>::iterator process = this->process.begin() ; process != this->process.end(); ++process) {
            std::string identifier = process->first;

            RaMatchResult resultProcess = this->processFrame(frame, identifier, keypoints, descriptors);
            if (resultProcess.existsMatch && resultProcess.stats.matches > result.stats.matches) {
                result = resultProcess;
            }
        }
    }
    this->queueMutex.unlock();

    return result;
}

RaMatchResult RaRobustMatcher::detectObject(const cv::Mat &frame)
{
    this->queueMutex.lock();
    RaMatchResult result;
    std::vector<cv::KeyPoint> keypoints;
    cv::Mat descriptors;
    std::vector<cv::Point2f> points;
    
    this->descriptorFinder.computeImage(frame, keypoints, descriptors);
    if (this->isTracking()) {
        result = this->processFrame(frame, this->layerIdentifier, keypoints, descriptors, points);
    } else {
        for (std::map<std::string , RaProcessFrame>::iterator process = this->process.begin() ; process != this->process.end(); ++process) {
            
            std::string identifier = process->first;
            RaMatchResult resultProcess = this->processFrame(frame, identifier, keypoints, descriptors);
            if (resultProcess.existsMatch && resultProcess.stats.matches > result.stats.matches) {
                result = resultProcess;
            }
        }
    }
    this->queueMutex.unlock();
    return result;
}


RaMatchResult RaRobustMatcher::processFrame(const cv::Mat &frame, const std::string &identifier, const std::vector<cv::KeyPoint> &keypoints, const cv::Mat &descriptors, std::vector<cv::Point2f> &points)
{
    RaMatchResult result;
    
    RaProcessFrame processFrame = this->process.at(identifier);

    processFrame.setFrameDescriptors(keypoints, descriptors);
    RaMatchResult resultProcess = processFrame.processFrame(frame);
    resultProcess.identifier = identifier;
    
    std::vector<cv::Point2f> imagePoints = processFrame.getRobustMatcher().getImageCornersPointWithPerspective();
    points.clear();
    std::copy(imagePoints.begin(), imagePoints.end(), back_inserter(points));
    
    return resultProcess;
}

RaMatchResult RaRobustMatcher::processFrame(const cv::Mat &frame, const std::string &identifier, const std::vector<cv::KeyPoint> &keypoints, const cv::Mat &descriptors)
{
    RaMatchResult result;
    
    RaProcessFrame processFrame = this->process.at(identifier);
    
    processFrame.setFrameDescriptors(keypoints, descriptors);
    RaMatchResult resultProcess = processFrame.processFrame(frame);
    resultProcess.identifier = identifier;
    
    return resultProcess;
}


void RaRobustMatcher::enabledTracking(std::string &objectIdentifier)
{
    this->layerIdentifier = objectIdentifier;
    this->tracking = true;
}

void RaRobustMatcher::disabledTracking()
{
    this->tracking = false;
}

bool RaRobustMatcher::isTracking()
{
    return this->tracking;
}
