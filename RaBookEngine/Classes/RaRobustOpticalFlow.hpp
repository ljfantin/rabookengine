//
//  RaRobustOpticalFlow.hpp
//  Pods
//
//  Created by Leandro Fantin on 5/7/17.
//
//

#ifndef RaRobustOpticalFlow_hpp
#define RaRobustOpticalFlow_hpp

#include <stdio.h>
#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"

class RaRobustOpticalFlow {
    
private:
    
    std::vector<cv::Point2f> previousPoints;
    cv::Mat previousFrame;
    cv::RotatedRect rect;
public:

    RaRobustOpticalFlow();
    void setPreviousFrame(const cv::Mat&frame);
    void setPreviousPoints(std::vector<cv::Point2f>&points);
    std::vector<cv::Point2f> getPreviousPoints();
    void calculateFirstPoints(const cv::Mat&frame);
    bool calculateOpticalFlow(const cv::Mat&frame, std::vector<cv::Point2f>&points);
    void reset();
};

#endif /* RaRobustOpticalFlow_hpp */
