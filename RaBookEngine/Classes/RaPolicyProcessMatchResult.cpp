//
//  RaPolicyProcessMatchResult.cpp
//  RaBook
//
//  Created by Leandro Fantin on 28/8/16.
//  Copyright © 2016 com.piantao. All rights reserved.
//

#include "RaPolicyProcessMatchResult.hpp"

void RaPolicyProcessMatchResult::processResultAndDecideIfExistsMatch(RaMatchResult &result)
{
    // antes era > 6
    result.existsMatch = (result.stats.matches > 20);
}
