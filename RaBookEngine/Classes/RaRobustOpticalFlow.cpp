//
//  RaRobustOpticalFlow.cpp
//  Pods
//
//  Created by Leandro Fantin on 5/7/17.
//
//

#include "RaRobustOpticalFlow.hpp"
#include <opencv2/video/video.hpp>

RaRobustOpticalFlow::RaRobustOpticalFlow()
{

}

void RaRobustOpticalFlow::setPreviousFrame(const cv::Mat&frame)
{
    cv::Mat frameGray;
    cvtColor(frame,frameGray,CV_RGB2GRAY);
    frameGray.copyTo(this->previousFrame);
}

std::vector<cv::Point2f> RaRobustOpticalFlow::getPreviousPoints()
{
    return this->previousPoints;
}


void RaRobustOpticalFlow::setPreviousPoints(std::vector<cv::Point2f>&points)
{
    this->previousPoints.clear();
    this->previousPoints.reserve(points.size());
    std::copy(points.begin(), points.end(), back_inserter(this->previousPoints));
}

bool RaRobustOpticalFlow::calculateOpticalFlow(const cv::Mat&frame, std::vector<cv::Point2f>&points)
{
    cv::Mat newFrame;
    cvtColor(frame,newFrame,CV_RGB2GRAY);
    if (!this->previousFrame.empty() && this->previousPoints.size()>0) {

        std::vector<uchar> status;
        std::vector<float> errors;
        std::vector<cv::Point2f> resultPoints;

        bool pointDetectedFlags[4] = {false, false, false, false};
        std::vector<cv::Point2f> temp;
        std::vector<cv::Point2f> mergePoints;
        int countError = 0;
        cv::TermCriteria termcrit(cv::TermCriteria::COUNT|cv::TermCriteria::EPS,20,0.03);
        cv::Size subPixWinSize(10,10);
        cv::Size winSize(31,31);
        
        calcOpticalFlowPyrLK(this->previousFrame, newFrame, this->previousPoints, points, status, errors, winSize,
                             3, termcrit, 0, 0.001);
       
        for (int i = 0; i < status.size(); ++i) {
            
            if (!status[i]) {
                temp.push_back(this->previousPoints[i]);
                countError++;
                continue;
            }

            if (cv::norm(this->previousPoints[i] - points[i]) <= 4) {
                temp.push_back(this->previousPoints[i]);
                continue;
            }
            
            resultPoints.push_back(points[i]);
            pointDetectedFlags[i] = true;
        }
        
        if (countError >=1) {
            return false;
        }
        
        if (temp.size() > 0) {
            
            cornerSubPix(this->previousFrame, temp, subPixWinSize, cv::Size(-1,-1), termcrit);
            // merge
            int indexTemp = 0;
            int indexResultPoint = 0;
            for (int index = 0; index < 4; index++) {
                if (pointDetectedFlags[index]) {
                    mergePoints.push_back(resultPoints[indexResultPoint]);
                    indexResultPoint++;
                } else {
                    mergePoints.push_back(temp[indexTemp]);
                    indexTemp++;
                }
            }
            
            points.clear();
            this->previousPoints.clear();
            
            std::copy(mergePoints.begin(), mergePoints.end(), back_inserter(points));
            std::copy(mergePoints.begin(), mergePoints.end(), back_inserter(this->previousPoints));

        } else {
            points.clear();
            this->previousPoints.clear();
            std::copy(resultPoints.begin(), resultPoints.end(), back_inserter(points));
            std::copy(resultPoints.begin(), resultPoints.end(), back_inserter(this->previousPoints));
        }
        
        return (points.size() == 4);
    }
    return false;
}

void RaRobustOpticalFlow::calculateFirstPoints(const cv::Mat&frame)
{
    this->setPreviousFrame(frame);
    std::vector<cv::Point2f> points;
    goodFeaturesToTrack(this->previousFrame, points, 500, 0.01, 10, cv::Mat(), 3, 0, 0.04);
    cv::TermCriteria termcrit(cv::TermCriteria::COUNT|cv::TermCriteria::EPS,20,0.03);
    cv::Size subPixWinSize(10,10);
    cornerSubPix(this->previousFrame, points, subPixWinSize, cv::Size(-1,-1), termcrit);
    this->setPreviousPoints(points);
}

void RaRobustOpticalFlow::reset()
{
    this->previousPoints.clear();
}


