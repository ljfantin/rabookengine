//
//  RaImageRobustDescriptorMatcher.cpp
//  RaBook
//
//  Created by Leandro Fantin on 13/8/16.
//  Copyright © 2016 com.piantao. All rights reserved.
//

#include "RaImageRobustDescriptorMatcher.hpp"
#include <time.h>
#include <iostream>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgproc.hpp>


RaImageRobustDescriptorMatcher::RaImageRobustDescriptorMatcher()
{
    cv::Ptr<cv::DescriptorMatcher> matcher = cv::DescriptorMatcher::create("BruteForce-Hamming");
    this->setDescriptorMatcher(matcher);
    this->ratio = 0.75f;
    this->imagePointsCornersWithPerspective = std::vector<cv::Point2f>(4);
}


int RaImageRobustDescriptorMatcher::ratioTest(std::vector<std::vector<cv::DMatch> > &matches)
{
    int removed = 0;
    // for all matches
    for ( std::vector<std::vector<cv::DMatch> >::iterator
         matchIterator= matches.begin(); matchIterator!= matches.end(); ++matchIterator)
    {
        // if 2 NN has been identified
        if (matchIterator->size() > 1)
        {
            // check distance ratio   min_dist?????
            if ((*matchIterator)[0].distance / (*matchIterator)[1].distance > this->ratio)
            {
                matchIterator->clear(); // remove match
                removed++;
            }
        }
        else
        { // does not have 2 neighbours
            matchIterator->clear(); // remove match
            removed++;
        }
    }
    return removed;
}

void RaImageRobustDescriptorMatcher::symmetryTest( const std::vector<std::vector<cv::DMatch> >& matches1,
                                 const std::vector<std::vector<cv::DMatch> >& matches2,
                                 std::vector<cv::DMatch>& symMatches )
{
    
    // for all matches image 1 -> image 2
    for (std::vector<std::vector<cv::DMatch> >::const_iterator
         matchIterator1 = matches1.begin(); matchIterator1 != matches1.end(); ++matchIterator1)
    {
        
        // ignore deleted matches
        if (matchIterator1->empty() || matchIterator1->size() < 2)
            continue;
        
        // for all matches image 2 -> image 1
        for (std::vector<std::vector<cv::DMatch> >::const_iterator
             matchIterator2 = matches2.begin(); matchIterator2 != matches2.end(); ++matchIterator2)
        {
            // ignore deleted matches
            if (matchIterator2->empty() || matchIterator2->size() < 2)
                continue;
            
            // Match symmetry test
            if ((*matchIterator1)[0].queryIdx ==
                (*matchIterator2)[0].trainIdx &&
                (*matchIterator2)[0].queryIdx ==
                (*matchIterator1)[0].trainIdx)
            {
                // add symmetrical match
                symMatches.push_back(
                                     cv::DMatch((*matchIterator1)[0].queryIdx,
                                                (*matchIterator1)[0].trainIdx,
                                                (*matchIterator1)[0].distance));
                break; // next match in image 1 -> image 2
            }
        }
    }
    
}

void RaImageRobustDescriptorMatcher::setDescriptorMatcher(cv::Ptr<cv::DescriptorMatcher>& descMatcher)
{
    this->matcher = descMatcher;
}

void RaImageRobustDescriptorMatcher::setRatio(float ratio)
{
    this->ratio = ratio;
}

void RaImageRobustDescriptorMatcher::setObjectImageDescriptors(int imageWidth, int imageHeight, const std::vector<cv::KeyPoint>& keypoints, const cv::Mat& descriptors)
{
    this->imageWidth = imageWidth;
    this->imageHeight = imageHeight;
    this->objectImageKeypoints = keypoints;
    this->objectImageDescriptors = descriptors;
}

void RaImageRobustDescriptorMatcher::setSceneImageDescriptors(const std::vector<cv::KeyPoint>& keypoints, const cv::Mat& descriptors)
{
    this->sceneImageKeypoints = keypoints;
    this->sceneImageDescriptors = descriptors;
}

std::vector<cv::Point2f> const & RaImageRobustDescriptorMatcher::getImagePointsWithPerspective()
{
    return this->imagePointsWithPerpspective;
}

std::vector<cv::Point2f>  const & RaImageRobustDescriptorMatcher::getImageCornersPointWithPerspective()
{
    return this->imagePointsCornersWithPerspective;
}

bool RaImageRobustDescriptorMatcher::calculateHomography(std::vector<cv::DMatch>& good_matches,std::vector<cv::KeyPoint>& imageKeypoints,std::vector<cv::KeyPoint>& frameKeypoints)
{
    if (good_matches.size() > 4) {
        std::vector<cv::Point2f> obj;
        std::vector<cv::Point2f> scene;
        for( int i = 0; i <  good_matches.size(); i++ )
        {
            obj.push_back( imageKeypoints[  good_matches[i].queryIdx ].pt );
            scene.push_back( frameKeypoints[ good_matches[i].trainIdx ].pt );
        }
        
        //cv::Mat homographyPresent = findHomography( obj, scene, CV_RANSAC, 2.5f, this->inlier_mask);
        /*cv::Mat homographyPresent = findHomography( obj, scene, CV_RANSAC, 3.f, this->inlier_mask);
        if (homography.empty()) {
            homographyPresent.copyTo(homography);
          //  return;
        }
        if (homography.empty() || homographyPresent.empty()) {
            return false;
        }
        accumulateWeighted(homographyPresent, homography, 0.1);*/
        
        homography = findHomography(obj, scene,
                                   cv::RANSAC, 3.f, this->inlier_mask);
        
        return true;
    }
    return false;
}

void RaImageRobustDescriptorMatcher::calculateImagePointsWithPerspective(std::vector<cv::DMatch>& good_matches,std::vector<cv::KeyPoint>& keypointsFrame)
{
    if (!homography.empty()) {
        std::vector<cv::Point2f> obj;
        for( int i = 0; i <  good_matches.size(); i++ )
        {
            obj.push_back( keypointsFrame[  good_matches[i].queryIdx ].pt );
        }
        this->imagePointsWithPerpspective.clear();
        perspectiveTransform( obj, this->imagePointsWithPerpspective, this->homography);
    }
}

void RaImageRobustDescriptorMatcher::calculateImagePointsCornerWithPerspective()
{
    if (!homography.empty()) {
        // get the corners from the object image
        std::vector<cv::Point2f> obj_corners(4);
        obj_corners[0] = cv::Point2f(0,0);
        obj_corners[1] = cv::Point2f( this->imageWidth, 0 );
        obj_corners[2] = cv::Point2f( this->imageWidth, this->imageHeight );
        obj_corners[3] = cv::Point2f( 0, this->imageHeight );
    
        // calculate perspective transformation of object corners within scene
        this->imagePointsCornersWithPerspective.clear();
        perspectiveTransform( obj_corners, this->imagePointsCornersWithPerspective, this->homography);
    }
}

void RaImageRobustDescriptorMatcher::match(const cv::Mat& frame, std::vector<cv::DMatch>& good_matches, Stats &stats)
{
    good_matches.clear();

    // 2. Match the two image descriptors
    std::vector<std::vector<cv::DMatch> > matches12, matches21;
    
    if (this->sceneImageDescriptors.cols == 0 && this->sceneImageDescriptors.rows == 0) {
        return;
    }
    // 2a. From image 1 to image 2
    this->matcher->knnMatch(this->objectImageDescriptors, this->sceneImageDescriptors , matches12, 2); // return 2 nearest neighbours
    if (this->objectImageDescriptors.cols!=this->sceneImageDescriptors.cols){
        return;
    }
    
    // 2b. From image 2 to image 1
    this->matcher->knnMatch(this->sceneImageDescriptors, this->objectImageDescriptors, matches21, 2); // return 2 nearest neighbours
    
    // 3. Remove matches for which NN ratio is > than threshold
    // clean image 1 -> image 2 matches
    ratioTest(matches12);
    
    // clean image 2 -> image 1 matches
    ratioTest(matches21);
    
    // 4. Remove non-symmetrical matches
    symmetryTest(matches12, matches21, good_matches);
    
    this->imagePointsWithPerpspective.clear();
    this->imagePointsCornersWithPerspective.clear();

    if (calculateHomography(good_matches,this->objectImageKeypoints, this->sceneImageKeypoints)) {
        if (this->inlier_mask.rows != 0 || this->inlier_mask.cols != 0) {
            for(unsigned i = 0; i < this->objectImageKeypoints.size(); i++) {
                if(this->inlier_mask.at<uchar>(i)) {
                    int new_i = static_cast<int>(inliers1.size());
                    inliers1.push_back(this->objectImageKeypoints[i]);
                    inliers2.push_back(this->sceneImageKeypoints[i]);
                    inlier_matches.push_back(cv::DMatch(new_i, new_i, 0));
                }
            }
            stats.inliers = (int)inliers1.size();
            stats.ratio = ((float)stats.inliers) / stats.matches;
            stats.matches = (int)good_matches.size();
            stats.keypoints = (int)this->sceneImageKeypoints.size();

            calculateImagePointsWithPerspective(inlier_matches, inliers1);
            calculateImagePointsCornerWithPerspective();
        }
    } 
}




