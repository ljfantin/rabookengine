//
//  RaImageRobustDescriptorMatcher.hpp
//  RaBook
//
//  Created by Leandro Fantin on 13/8/16.
//  Copyright © 2016 com.piantao. All rights reserved.
//

#ifndef RaImageRobustMatcher_hpp
#define RaImageRobustMatcher_hpp

#include <stdio.h>
#include <opencv2/features2d/features2d.hpp>
#include "RaMatchResult.hpp"


class RaImageRobustDescriptorMatcher {
private:

    // Object
    int imageWidth;
    int imageHeight;
    cv::Mat objectImageDescriptors;
    std::vector<cv::KeyPoint> objectImageKeypoints;
    
    std::vector<cv::Point2f> imagePointsWithPerpspective;
    std::vector<cv::Point2f> imagePointsCornersWithPerspective;
    
    // Scene
    cv::Mat sceneImageDescriptors;
    std::vector<cv::KeyPoint> sceneImageKeypoints;
    
    // homography
    cv::Mat homography;
    cv::Mat inlier_mask;
    std::vector<cv::KeyPoint> inliers1, inliers2;
    std::vector<cv::DMatch> inlier_matches;
    
    // Matcher
    float ratio;
    cv::Ptr<cv::DescriptorMatcher> matcher;

    // Metodos privados
    int ratioTest(std::vector<std::vector<cv::DMatch> > &matches);
    int ratioTestWithMinimunDistance(std::vector<std::vector<cv::DMatch> > &matches);

    void symmetryTest( const std::vector<std::vector<cv::DMatch> >& matches1,
                                       const std::vector<std::vector<cv::DMatch> >& matches2,
                                       std::vector<cv::DMatch>& symMatches);
    
    std::vector<cv::Point2f> Points(std::vector<cv::KeyPoint> keypoints);
    
    bool calculateHomography(std::vector<cv::DMatch>& good_matches,std::vector<cv::KeyPoint>& imageKeypoints,std::vector<cv::KeyPoint>& frameKeypoints);
    void calculateImagePointsCornerWithPerspective();
    void calculateImagePointsWithPerspective(std::vector<cv::DMatch>& good_matches,std::vector<cv::KeyPoint>& keypoints_frame);
    
public:
    RaImageRobustDescriptorMatcher();
    void setDescriptorMatcher(cv::Ptr<cv::DescriptorMatcher>& descMatcher);
    void setRatio(float ratio);
    void setObjectImageDescriptors(int imageWidth, int imageHeight, const std::vector<cv::KeyPoint>& keypoints, const cv::Mat& descriptors);
    void setSceneImageDescriptors(const std::vector<cv::KeyPoint>& keypoints, const cv::Mat& descriptors);

    
    void match(const cv::Mat& frame, std::vector<cv::DMatch>& good_matches, Stats &stats);

    
    std::vector<cv::Point2f>  const & getImagePointsWithPerspective();
    std::vector<cv::Point2f>  const & getImageCornersPointWithPerspective();
};

#endif /* RaImageRobustMatcher_hpp */
