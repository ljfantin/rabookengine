//
//  RaPreProcessColorFilterFrame.cpp
//  RaBook
//
//  Created by Leandro Fantin on 21/8/16.
//  Copyright © 2016 com.piantao. All rights reserved.
//

#include "RaPreProcessColorFilter.hpp"
#include "opencv2/imgproc.hpp"


void RaPreProcessColorFilter::applyFilter(cv::Mat& image)
{
    cvtColor(image, image, CV_BGR2RGB);
}
