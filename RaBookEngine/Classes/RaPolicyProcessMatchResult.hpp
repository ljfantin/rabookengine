//
//  RaPolicyProcessMatchResult.hpp
//  RaBook
//
//  Created by Leandro Fantin on 28/8/16.
//  Copyright © 2016 com.piantao. All rights reserved.
//

#ifndef RaPolicyProcessMatchResult_hpp
#define RaPolicyProcessMatchResult_hpp

#include "RaMatchResult.hpp"
#include <stdio.h>

class RaPolicyProcessMatchResult {
    
public:

    void processResultAndDecideIfExistsMatch(RaMatchResult &result);
};

#endif /* RaPolicyProcessMatchResult_hpp */
