//
//  RaFileStorage.cpp
//  RaBook
//
//  Created by Leandro Fantin on 1/9/16.
//  Copyright © 2016 com.piantao. All rights reserved.
//

#include "RaFileStorage.hpp"

void RaFileStorage::saveToFileSystem(std::string name,std::vector<cv::KeyPoint>& keypoints, cv::Mat & descriptors)
{
    cv::FileStorage fs(name, cv::FileStorage::WRITE);
    write( fs , "keypoints", keypoints );
    write( fs , "descriptors", descriptors );
    fs.release();
}

void RaFileStorage::readFromFileSystem(std::string name,std::vector<cv::KeyPoint>& keypoints,cv::Mat &descriptors)
{
    cv::FileStorage fs(name, cv::FileStorage::READ);
    fs["keypoints"] >> keypoints;
    fs["descriptors"] >> descriptors;
    fs.release();
}

void RaFileStorage::printFile(std::string name)
{
    std::ifstream inFile;
    inFile.open(name);
    std::string line;
    while(!inFile.eof())
    {
        //Get input
        inFile >> line;
        
        //Print input
        std::cout << line;
    } 
    
    //Close file 
    inFile.close();
}

