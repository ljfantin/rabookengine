//
//  RaProcessFrame.cpp
//  RaBook
//
//  Created by Leandro Fantin on 14/8/16.
//  Copyright © 2016 com.piantao. All rights reserved.
//

#include "RaProcessFrame.hpp"
#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/core/types.hpp"


RaMatchResult RaProcessFrame::processFrame(const cv::Mat &frame)
{
    std::vector<cv::DMatch> good_matches;
    Stats statsframe;
    this->robustMatcher.match(frame, good_matches, statsframe);
    std::vector<cv::Point2f> keypoints = this->robustMatcher.getImagePointsWithPerspective();
    this->result.corners.clear();
    this->result.stats = statsframe;
    this->processResult.processResultAndDecideIfExistsMatch(this->result);
    if (this->result.existsMatch) {
        
        if (good_matches.size() > 0) {
            minEnclosingCircle(keypoints,this->result.center,this->result.radius);
            cv::Rect box = cv::boundingRect(cv::Mat(keypoints));
            this->result.rect = box;
        }

        if (this->robustMatcher.getImageCornersPointWithPerspective().size() > 0) {
            this->result.corners = this->robustMatcher.getImageCornersPointWithPerspective();
        }
    }
    return this->result;
}

void RaProcessFrame::setObjectImageDescriptors(int width, int height, std::vector<cv::KeyPoint> &keypoints,cv::Mat &descriptors)
{
    this->robustMatcher.setObjectImageDescriptors(width, height, keypoints, descriptors);
}

void RaProcessFrame::setFrameDescriptors(const std::vector<cv::KeyPoint> &keypoints,const cv::Mat &descriptors)
{
    this->robustMatcher.setSceneImageDescriptors(keypoints, descriptors);
}

void RaProcessFrame::setLayerDescriptors(LayerType type, const std::string &identifier)
{
    this->result.layerIdentifier = identifier;
    this->result.layerType = type;
}

