//
//  RaComputeImage.hpp
//  RaBook
//
//  Created by Leandro Fantin on 3/9/16.
//  Copyright © 2016 com.piantao. All rights reserved.
//

#ifndef RaImageDescriptorFinder_hpp
#define RaImageDescriptorFinder_hpp

#include <stdio.h>
#include <stdio.h>
#include <opencv2/features2d/features2d.hpp>

class RaImageDescriptorFinder {
private:
    // pointer to the feature point detector object
    cv::Ptr<cv::FeatureDetector> detector;
    // pointer to the feature descriptor extractor object
    cv::Ptr<cv::DescriptorExtractor> extractor;
    
    void computeDescriptors( const cv::Mat& image, std::vector<cv::KeyPoint>& keypoints, cv::Mat& descriptors);
    void computeKeyPoints( const cv::Mat& image, std::vector<cv::KeyPoint>& keypoints);
    void computeKeyPoints( const cv::Mat& image, std::vector<cv::KeyPoint>& keypoints, const cv::Mat& mask);
    void setFeatureDetector(cv::Ptr<cv::FeatureDetector>& detect);
    void setDescriptorExtractor(cv::Ptr<cv::DescriptorExtractor>& desc);

public:
    RaImageDescriptorFinder();
    void computeImage(const cv::Mat& image, std::vector<cv::KeyPoint>& keypoints, cv::Mat& descriptors);
    void computeImage(const cv::Mat& image, const cv::Mat& mask,std::vector<cv::KeyPoint>& keypoints, cv::Mat& descriptors);

};
#endif /* RaImageDescriptorFinder_hpp */
