//
//  RaFileStorage.hpp
//  RaBook
//
//  Created by Leandro Fantin on 1/9/16.
//  Copyright © 2016 com.piantao. All rights reserved.
//

#ifndef RaFileStorage_hpp
#define RaFileStorage_hpp

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <opencv2/opencv.hpp>

class RaFileStorage {
    
public:
    void saveToFileSystem(std::string name,std::vector<cv::KeyPoint>& keypoints, cv::Mat & descriptors);
    void readFromFileSystem(std::string name,std::vector<cv::KeyPoint>& keypoints,cv::Mat & descriptors);
    void printFile(std::string name);
};

#endif /* RaFileStorage_hpp */
