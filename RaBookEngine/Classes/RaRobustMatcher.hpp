//
//  RaRobustMatcher.hpp
//  RaBook
//
//  Created by Leandro Fantin on 28/8/16.
//  Copyright © 2016 com.piantao. All rights reserved.
//

#ifndef RaRobustMatcher_hpp
#define RaRobustMatcher_hpp

#include <stdio.h>
#include "opencv2/core.hpp"
#include "RaProcessFrame.hpp"
#include "RaPreProcessColorFilter.hpp"
#include <map>
#include "RaRobustOpticalFlow.hpp"

/// Clase que tiene todos las instancias de RaProcessFrame una por imagen a detectar imagen
class RaRobustMatcher {

private:
    std::map<std::string,RaProcessFrame> process;
    RaPreProcessColorFilter preProcess;
    RaImageDescriptorFinder descriptorFinder;
    std::mutex queueMutex;
    
    static RaRobustMatcher *instance;
    RaRobustMatcher();
    ~RaRobustMatcher();

    std::string layerIdentifier;
    LayerType layerType;

    bool tracking;
    
    RaRobustOpticalFlow opticalFlow;
    
    RaMatchResult lastResult;
    
    RaMatchResult processFrame(const cv::Mat &frame, const std::string &identifier, const std::vector<cv::KeyPoint> &keypoints, const cv::Mat &descriptors, std::vector<cv::Point2f> &points);
    RaMatchResult processFrame(const cv::Mat &frame, const std::string &identifier, const std::vector<cv::KeyPoint> &keypoints, const cv::Mat &descriptors);
public:
    /**
     * Singleton
     */
    static RaRobustMatcher * getRobustMatcherSharedInstance();
    
    void processImages(std::map<std::string ,cv::Mat> &images);
    void processImage(std::string identifier, cv::Mat &image);
    void processImageAndSave(cv::Mat &image, std::string &temp);
    void addProcessedImage(const std::string &identifier, int width, int height, std::vector<cv::KeyPoint> &keypoints, cv::Mat &descriptors, LayerType layerType, const std::string &layerIdentifier);
    void removeProcessedImage(std::string & identifier);
    void removeAllProcessedImage();
    
    
    void enabledTracking(std::string &objectIdentifier);
    void disabledTracking();
    bool isTracking();
    
    RaMatchResult detectAndTrackObject(const cv::Mat &frame);
    RaMatchResult detectAndTrackObject(const cv::Mat &frame, const cv::Mat &mask);
    RaMatchResult detectObject(const cv::Mat &frame);
    RaMatchResult detectObject(const cv::Mat &frame, const cv::Mat &mask);

};

#endif /* RaRobustMatcher_hpp */
