//
//  RaProcessFrame.hpp
//  RaBook
//
//  Created by Leandro Fantin on 14/8/16.
//  Copyright © 2016 com.piantao. All rights reserved.
//

#ifndef RaProcessFrame_hpp
#define RaProcessFrame_hpp

#include <stdio.h>
#include <opencv2/core.hpp>
#include "RaImageRobustDescriptorMatcher.hpp"
#include "RaMatchResult.hpp"
#include "RaPolicyProcessMatchResult.hpp"
#include "RaImageDescriptorFinder.hpp"

class RaProcessFrame {
private:
    RaImageRobustDescriptorMatcher robustMatcher;
    RaMatchResult result;
    RaPolicyProcessMatchResult processResult;
    
public:
    void setObjectImageDescriptors(int width, int height, std::vector<cv::KeyPoint> &keypoints,cv::Mat &descriptors);
    void setFrameDescriptors(const std::vector<cv::KeyPoint> &keypoints,const cv::Mat &descriptors);
    void setLayerDescriptors(LayerType type, const std::string &identifier);
    inline RaImageRobustDescriptorMatcher & getRobustMatcher() { return this->robustMatcher;}
    RaMatchResult processFrame(const cv::Mat &frame);
};

#endif /* RaProcessFrame_hpp */
