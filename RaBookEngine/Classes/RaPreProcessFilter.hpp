//
//  RaPreProcessFilter.hpp
//  RaBook
//
//  Created by Leandro Fantin on 3/9/16.
//  Copyright © 2016 com.piantao. All rights reserved.
//

#ifndef RaPreProcessFilter_hpp
#define RaPreProcessFilter_hpp

#include "opencv2/core.hpp"

class RaPreProcessFilter {
    
public:
    virtual void applyFilter(cv::Mat& image) = 0;
};
#endif /* RaPreProcessFilter_hpp */
