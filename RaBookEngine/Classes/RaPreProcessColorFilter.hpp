//
//  RaPreProcessColorFilter.hpp
//  RaBook
//
//  Created by Leandro Fantin on 21/8/16.
//  Copyright © 2016 com.piantao. All rights reserved.
//

#ifndef RaPreProcessColorFilter_hpp
#define RaPreProcessColorFilter_hpp

#include "RaPreProcessFilter.hpp"

class RaPreProcessColorFilter:public RaPreProcessFilter {
    
public:
    virtual void applyFilter(cv::Mat& image);
};

#endif /* RaPreProcessColorFilter_hpp */
