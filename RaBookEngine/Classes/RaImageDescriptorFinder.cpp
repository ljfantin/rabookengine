//
//  RaImageDescriptorFinder.cpp
//  RaBook
//
//  Created by Leandro Fantin on 3/9/16.
//  Copyright © 2016 com.piantao. All rights reserved.
//

#include "RaImageDescriptorFinder.hpp"
#include <time.h>
#include <iostream>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgproc.hpp>

RaImageDescriptorFinder::RaImageDescriptorFinder()
{
    int nfeatures = 500;
    float scaleFactor = 1.2f;
    int nlevels = 8;
    int edgeThreshold = 31;
    int firstLevel = 0;
    int WTA_K = 2;
    int scoreType = cv::ORB::HARRIS_SCORE;
    int patchSize = 31;
    int fastThreshold = 20;
    
    cv::Ptr<cv::FeatureDetector> orb = cv::ORB::create(nfeatures,
                                                       scaleFactor,
                                                       nlevels,
                                                       edgeThreshold,
                                                       firstLevel,
                                                       WTA_K,
                                                       scoreType,
                                                       patchSize,
                                                       fastThreshold);
    this->setFeatureDetector(orb);
    this->setDescriptorExtractor(orb);
}


void RaImageDescriptorFinder::computeDescriptors( const cv::Mat& image, std::vector<cv::KeyPoint>& keypoints, cv::Mat& descriptors)
{
    extractor->compute(image, keypoints, descriptors);
}

void RaImageDescriptorFinder::computeKeyPoints( const cv::Mat& image, std::vector<cv::KeyPoint>& keypoints)
{
    detector->detect(image, keypoints);
}

void RaImageDescriptorFinder::computeKeyPoints( const cv::Mat& image, std::vector<cv::KeyPoint>& keypoints,const cv::Mat& mask)
{
    detector->detect(image, keypoints, mask);
}

void RaImageDescriptorFinder::setFeatureDetector(cv::Ptr<cv::FeatureDetector>& detect)
{
    this->detector = detect;
}

void RaImageDescriptorFinder::setDescriptorExtractor(cv::Ptr<cv::DescriptorExtractor>& desc)
{
    this->extractor = desc;
}

void RaImageDescriptorFinder::computeImage(const cv::Mat& image, std::vector<cv::KeyPoint>& keypoints, cv::Mat& descriptors)
{
    // 1a. Detection of the features
    this->computeKeyPoints(image, keypoints);
    
    // 1b. Extraction of the descriptors
    this->computeDescriptors(image, keypoints, descriptors);
}

void RaImageDescriptorFinder::computeImage(const cv::Mat& image, const cv::Mat& mask, std::vector<cv::KeyPoint>& keypoints, cv::Mat& descriptors)
{
    // 1a. Detection of the features
    this->computeKeyPoints(image, keypoints, mask);
    
    // 1b. Extraction of the descriptors
    this->computeDescriptors(image, keypoints, descriptors);
}


