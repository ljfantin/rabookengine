//
//  RaMatchResult.hpp
//  RaBook
//
//  Created by Leandro Fantin on 25/8/16.
//  Copyright © 2016 com.piantao. All rights reserved.
//

#ifndef RaMatchResult_hpp
#define RaMatchResult_hpp

#include <stdio.h>
#include "opencv2/core.hpp"

struct Stats
{
    int matches;
    int inliers;
    double ratio;
    int keypoints;
    
    Stats() : matches(0),
    inliers(0),
    ratio(0),
    keypoints(0)
    {}
    
    Stats& operator+=(const Stats& op) {
        matches += op.matches;
        inliers += op.inliers;
        ratio += op.ratio;
        keypoints += op.keypoints;
        return *this;
    }
    Stats& operator/=(int num)
    {
        matches /= num;
        inliers /= num;
        ratio /= num;
        keypoints /= num;
        return *this;
    }
};

enum LayerType {
    Vimeo = 0,
    Object = 1,
    URL = 2
};

struct RaMatchResult
{
    Stats stats;
    std::vector<cv::Point2f> corners;
    cv::Point2f center;
    float radius;
    bool existsMatch = false;
    std::string identifier;
    std::string layerIdentifier;
    LayerType layerType;
    cv::Rect rect;
    
    bool isVimeoLayer() { return layerType == Vimeo; }
    bool isObjectLayer() { return layerType == Object; }
    bool isUrlLayer() { return layerType == URL; }
    
};

#endif /* RaMatchResult_hpp */
